#include "stdafx.h"
#include <iostream>

using namespace std;

class Queue {

	//The node of the stack
	struct Node {
		int data;
		Node *next;
	};

public:
	
	Queue() {
		head = NULL;
		tail = NULL;
	}

		~Queue() {
		Node *next = head;

		while (next) {
			Node *deleteMe = next;
			next = next->next;
			delete deleteMe; 
		}
	}


	int deQueue() {
		if (isEmpty()) 
			return 0;

		Node *newNode = head;
		int ret = newNode->data;

		head = head->next;
		delete newNode;
		return ret;
	}

	bool inQueue(int data) {
		Node * newNode = new Node();
		newNode->data = data;
		newNode->next = NULL;

		if (tail == NULL) {
			head = newNode;
			tail = head;
		}

		tail->next = newNode;

		tail = tail->next;

		return true;
	}

	
	bool isEmpty() {
		return head == NULL;
	}

private:
	Node * head;
	Node * tail;

};
int main()
{
	Queue * ll = new Queue();
	int n;
	int k;
	int d;
	int MyArr[4];

	for (int i = 0; i < 5; i++) {
		cout << "Set number:";
		cin >> n;
		MyArr[i] = n;
		ll->inQueue(n);
		if (i == 4) {
			cout << "---------" << endl;
		}
	}

	cout << "Inserted:" << " ";
	for (int i = 0; i < 5; i++)
	{
		cout << MyArr[i] << " ";
	}
		cout << "How many elements you want to left ->" << " ";
	cin >> d;
	for (int i = 0; i < d; i++)
	{
		cout << ll->deQueue() << endl;

	}

	//ll->~Queue();


	system("pause");
	return 0;
}

